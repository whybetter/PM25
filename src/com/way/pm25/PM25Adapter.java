package com.way.pm25;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.way.pm25.R;
import com.way.pm25.util.PM25Provider;

public class PM25Adapter extends BaseAdapter {
	private LayoutInflater mLayoutInflater;
	private List<PM25Provider.PM25> mList;

	public PM25Adapter(Context context, List<PM25Provider.PM25> pm25List) {
		mLayoutInflater = LayoutInflater.from(context);
		mList = pm25List;
	}

	public int getCount() {
		return -1 + mList.size();
	}

	public Object getItem(int paramInt) {
		return this.mList.get(paramInt);
	}

	public long getItemId(int paramInt) {
		return paramInt;
	}

	public View getView(int position, View view, ViewGroup parent) {
		PM25Provider.PM25 pm25Value = (PM25Provider.PM25) this.mList
				.get(position);
		Holder viewHolder;
		if (view == null) {
			view = this.mLayoutInflater.inflate(R.layout.paper_item, null);
			viewHolder = new Holder();
			viewHolder.mCityNameTextView = ((TextView) view
					.findViewById(R.id.paper_item_position_name));
			viewHolder.mAqiTextView = ((TextView) view
					.findViewById(R.id.paper_item_aqi));
			viewHolder.mPm25TextView = ((TextView) view
					.findViewById(R.id.paper_item_pm25));
			viewHolder.mPm10TextView = ((TextView) view
					.findViewById(R.id.paper_item_pm10));
			view.setTag(viewHolder);
		} else {
			viewHolder = (Holder) view.getTag();
		}
		viewHolder.mAqiTextView.setText(pm25Value.aqi);
		viewHolder.mPm25TextView.setText(pm25Value.pm2_5);
		viewHolder.mCityNameTextView.setText(pm25Value.position_name);
		viewHolder.mPm10TextView.setText(pm25Value.pm10);
		return view;
	}

	public static class Holder {
		TextView mAqiTextView;
		TextView mPm10TextView;
		TextView mPm25TextView;
		TextView mCityNameTextView;
	}
}
