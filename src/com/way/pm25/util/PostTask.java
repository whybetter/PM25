package com.way.pm25.util;

import android.os.AsyncTask;

public class PostTask extends AsyncTask<String, String, String> {
	private String mTargetURI;

	public PostTask(String targetUri) {
		this.mTargetURI = targetUri;
	}

	protected String doInBackground(String[] args) {
		return new APIClient().post(mTargetURI, args[0]);
	}
}